import axios from 'axios'
// const locals = ['localhost']
// const hostname = document.location.hostname
// const local = locals.includes(hostname)
// const isServer = hostname === '192.168.2.254'
// const pcJP = window.localStorage.getItem('pcJP')
export default async () => {
  // axios.defaults.baseURL = `http://${hostnmae}/iae/index.php?r=apiBuffet/`
  axios.defaults.baseURL = (process.env.NODE_ENV === 'production')
    ? '/iae/index.php?r=apiBuffet/'
    : 'http://localhost/iae/index.php?r=apiBuffet/'
  // axios.defaults.baseURL = `/iae/index.php?r=apiBuffet/`
  axios.interceptors.response.use((response) => {
    const endPoint = response.config.url.substring(response.config.url.lastIndexOf('/') + 1)
    console.log('endpoint %O %O response.data %O', endPoint, response, response.data)
    return response.data
  }, (error) => {
    Promise.reject(error)
  })

  axios.interceptors.request.use(
    async (config) => {
      return config
    },
    error => Promise.reject(error)
  )

  axios.defaults.method = 'POST'
}
