import yargs from 'yargs'
import addCommandsToYargs from './utils'
// axios.defaults.headers.common.Authorization = 'qpUBkTCkckgZWDqzxMaM'
export async function run () {
  addCommandsToYargs(yargs)
  const argv = yargs
    .parserConfiguration({ 'strip-aliased': true })
    // .option('verbose', { alias: 'v' })
    .help()
    .alias('help', 'h')
    .demandCommand()
    .strict()
    .argv

  const verbose = argv.verbose
  if (verbose) {
    console.log('--------------------\nVerbose\n--------------------')
    Object.keys(argv).forEach((element, i) => {
      console.log(element, argv[element])
    })
    console.log('--------------------')
  }
}
