/* eslint-disable no-unused-vars */
import sh from 'shelljs'
const del = require('del')
const parsedFiles = []
const fs = require('fs')

export default function (dep) {
  const dirnameExportImg = '/1/prg/recibo/server/recibos'
  const cmd = {}
  cmd.command = 'scp <carpeta>'
  cmd.desc = 'copia los pdf en <carpeta> al servidor'
  cmd.example = 'pdfReader scp pdfReader parse /home/jpsala/Documents/recibos/parsed'
  cmd.builder = yargs => {
    yargs.positional('carpeta', {
      type: 'string',
      desc: 'Carpeta donde enstán los pdf procesados',
      default: undefined
    })
  }
  cmd.handler = async function (argv) {
    const sourceDir = argv.carpeta
    const targetDir = 'iae.dyndns.org:/prg/recibo/server/recibos'
    // const pdfFiles = fs.readdirSync(sourceDir)
    const output = sh.exec(`scp -v ${sourceDir}/* ${targetDir}`, {
      async: false,
      silent: false
    })
      .stdout.replace(/\t/g, '')
      .split('\n')
    console.log('output', output)
  }
  return cmd
}
